/*
 * Copyright (c) Cangjie Library Team 2022-2022. All rights resvered.
 */

/**
 * @file
 * The file declars the struct Upgrader.
 */

package server

from net import http.HttpStatusCode
from encoding import base64.*
from encoding import url.*
from encoding import hex.*

let keyGuid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

/**
 * The struct is Upgrader
 * @author oldcat
 * @since 0.32.5
 */
public class Upgrader{

    // 暂时保留，用于兼容后续更新
    private var readBufSize:IntNative
    // 暂时保留，用于兼容后续更新
    private var writeBufSize:IntNative

    // 保留，方便后续更新
    private var enableCompression:Bool = false

    private var subprotocols = Array<String>()

    private var resp:Response

     /**
    * The Function is init constructor
    *
    * @param readBufSize of IntNative, 读取缓存
    * @param writeBufSize of IntNative, 写入缓存
    * @param resp of Response, Response
    * @since 0.32.5
    */
     init(readBufSize!:IntNative = 0,writeBufSize!:IntNative = 0,resp!:Response){
        this.resp = resp
        this.readBufSize = readBufSize
        this.writeBufSize = writeBufSize
    }


    /**
    * 服务端对客户端发送过来的握手信息校验的方法
    *
    * @param r of Requset
    *
    * @return Type of Bool
    * @since 0.32.5
    */
     func upgrade(r:Request):Bool {
        // println("2")
        let badHandshake = "websocket: the client is not using the websocket protocol: "
        // 检测请求头，此部分代码参考go的gorilla/websocket库
        if(!checkHeader(r.header(),"Connection", "upgrade")){
            return respError(HttpStatusCode.STATUS_BAD_REQUEST, "${badHandshake}'upgrade' token not found in 'Connection' header")
        }
        if(!checkHeader(r.header(),"Upgrade", "websocket")){
            return respError(HttpStatusCode.STATUS_BAD_REQUEST, "${badHandshake}'websocket' token not found in 'Upgrade' header")
        }
        if(r.methods() != "GET"){
            return respError(HttpStatusCode.STATUS_METHOD_NOT_ALLOWED, "${badHandshake}equest method is not GET")
        }
        //13的这种校验先去掉if(!checkHeader(r.header(),"Sec-WebSocket-Version", "13")){return respError(HttpStatusCode.STATUS_BAD_REQUEST, "websocket: unsupported version: 13 not found in 'Sec-Websocket-Version' header")}
        let challengeKey = r.getHeader("Sec-WebSocket-Key").getOrDefault({ => ""}).trimAscii()
        if(!validSecWebsocket4cjKey(challengeKey)){
            return respError(HttpStatusCode.STATUS_BAD_REQUEST, "websocket: not a websocket handshake: 'Sec-WebSocket-Key' header must be Base64 encoded value of 16-byte in length")
        }
        let key = getRespKey(challengeKey)
        resp.handleSuccess(key)
        // 方案一：等待标准库更新，通过升级http协议到websocket4cj的实现需要获取http原始连接对象
        // 方案二：使用tcp单独实现websocket4cj协议
    }


    /**
    * 对客户端发送过来的key进行规则加密的方法
    *
    * @param challengeKey of String
    *
    * @return Type of String
    * @since 0.32.5
    */
    private func getRespKey(challengeKey:String):String{
        let tmp = challengeKey + keyGuid
        let result = SHA1.digest(tmp)
        toBase64String(result)
    }


    /**
    * 校验客户端发送过来的key是否符合要求的方法
    *
    * @param key of String
    *
    * @return Type of Bool
    * @since 0.32.5
    */
    private func validSecWebsocket4cjKey(key:String):Bool{
        if(key == ""){
            return false
        }
        match(fromBase64String(key)){
            case Some(keyStr) => keyStr.size == 16
            case _ => false
        }
    }

    /**
    * 校验出错返回错误信息的方法
    *
    * @param code of Int64
    * @param msg of String
    *
    * @return Type of Bool
    * @since 0.32.5
    */
    private func respError(code:Int64,msg:String):Bool {
        this.resp.setHeader("Content-Type:", "text/html; charset=utf-8")
        this.resp.setHeader("Sec-Websocket-Version", "13")
        this.resp.writeStatusCode(code)
        this.resp.writeBodyAndClose(msg)
        return false
    }

    /**
    * 检查对于的请求头的值是否和自己预料的一样
    *
    * @param r of HashMap<String, String>
    * @param key of String
    * @param val of String
    *
    * @return Type of Bool
    * @since 0.32.5
    */
    private func checkHeader(r:HashMap<String, String>,key:String,val:String):Bool{
        let connection = r.get(key).getOrDefault({ => ""})
        if(connection == "" || connection.trimAscii().toAsciiLower() != val){
            return false
        }
        return true
    }
}


/**
* 判断客户端握手信息是否正确的方法
*
* @param w of Response
* @param r of Requset
* @param readBufSize of IntNative
* @param writeBufSize of IntNative
*
* @return Type of Bool
* @since 0.32.5
*/
 func ugrade(w:Response,r:Request,readBufSize!:IntNative,writeBufSize!:IntNative):Bool{
    var u = Upgrader(readBufSize:readBufSize,writeBufSize:writeBufSize,resp:w)
    u.upgrade(r)
}

