## websocket4cj 库

### 介绍
一个拥有websocket功能的仓颉版本库,这个库的协议握手部分实现是参照`gorilla/websocket`进行的,该库为websocket协议的实现（主要为协议握手、帧解析、包装等功能),参照地址https://github.com/gorilla/websocket

### 1 提供websocket4cj客户端与服务端的握手功能

前置条件：NA
场景：websocekt在运行的时候首先就是要握手，将http协议转化为websocekt协议
约束： NA
性能： 支持版本几何性能持平
可靠性： NA

#### 1.1 提供客户端握手功能
实现客户端和服务端的握手功能

##### 1.1.1 主要接口

class WebSocket4cjClient

```cangjie
    /*客户端握手连接的方法
     *
     *返回值 -  返回一个握手成功后返回的连接对象
     */
    public func connect():Option<WsConnClient>
```

#### 1.2 提供服务端的握手功能
实现客户端和服务端的握手功能

##### 1.2.1 主要接口

class WebSocket4cjServer

```cangjie
    /*用于启动服务端websocket4cj并握手
     *
     */
    public func launch()
```

class WsConn

```cangjie

    /*服务端获取客户端Method
     *
     *返回值 - 返回获取到的客户端Method
     */
    public func getMethod():String{

    /*服务端获取客户端url
     *
     *返回值 - 返回获取到的客户端url
     */
    public func getUrl():String{

    /*服务端获取客户端HttpVersion
     *
     *返回值 - 返回获取到的客户端HttpVersion
     */
    public func getHttpVersion():String{

    /*服务端获取客户端传过来Header
     *
     *返回值 - 返回获取到的客户端Header
     */
    public func getHeader():HashMap<String, String>{

```

##### 1.2.1.1 示例

```cangjie

from websocket4cj import client.*
from std import collection.*
from websocket4cj import server.*

var method = ""
var httpversion = ""
var url = ""
var headers = HashMap<String,String>()

main(){
    spawn{
        serve()
    }

    sleep(1000*1000*1000*1)
    let rawUrl = "ws://0.0.0.0:9170"
    let headerMap = HashMap<String, String>()
    headerMap.put("tag","client")
    var wsclient = WebSocket4cjClient(rawUrl,headerMap,10)
    let connClient:WsConnClient = wsclient.connect().getOrThrow()
    println("客户端握手成功")
    sleep(1000*1000*1000*1)
    if("GET".equals(method) && "HTTP/1.1".equals(httpversion) && headers.size>0){
        return 0
    }else{
        return 1
    }
}

public func serve(){
    var ws = WebSocket4cjServer("0.0.0.0",9170)
    ws.handleFunc{w=>
         method = w.getMethod()
         httpversion = w.getHttpVersion()
         url = w.getUrl()
         headers = w.getHeader()
        println("method="+method)
        println("httpversion="+httpversion)
        for(head in headers){
            println("key="+head[0]+" value="+head[1])
        }
        println("服务端握手成功")
    }
    ws.launch() // 启动监听
}
```

执行结果如下：

```shell
method=GET
httpversion=HTTP/1.1
key=Host value=0.0.0.0
key=Upgrade value=websocket
key=Connection value=Upgrade
key=Sec-WebSocket-Key value=bWVIMGEzTlNnNThhU3hXSA==
key=Origin value=ws
key=tag value=client
key=Sec-WebSocket-Version value=13
服务端握手成功
客户端握手成功
```

### 2 提供客户端与服务端的信息通信功能

前置条件：NA
场景：websocekt的核心能力就是客户端与服务端之间的通信，包括发送String文本，二进制字节数组，连续帧, ping帧 , close帧
约束：
性能：支持版本几何性能持平
可靠性： NA

#### 2.1 客户端与服务端的信息通信，包括发送文本
客户端和服务端可以互相发送文本

##### 2.1.1 主要接口

class WsConnClient

```cangjie
    /*客户端写消息发送到服务端的方法
     *
     *参数 arrayToWrite - 需要发送的数据
     */
    public func sendMsg(arrayToWrite: Array<UInt8>)

    /*客户端读取服务端发送过来消息的方法
     *
     * 返回值 - 返回一个元组类型 第一个参数是消息类型  第二个参数是读取到的字节数组
     */
    public func readMsg(): (MsgType,Option<Array<UInt8>>)
```

class WsConn

```cangjie
    /*服务端写消息发送到客户端的方法
     *
     *参数 arrayToWrite - 需要发送的数据
     */
    public func sendMsg(arrayToWrite: Array<UInt8>)

    /*服务端读取客户端发送过来消息的方法
     *
     * 返回值 - 返回一个元组类型 第一个参数是消息类型  第二个参数是读取到的字节数组
     */
    public func readMsg(): (MsgType,Option<Array<UInt8>>)
```

##### 2.1.1.1 示例

```cangjie

from websocket4cj import client.*
from std import collection.*
from websocket4cj import server.*

var clientStr = ""
var serverStr = ""

main(){

    spawn{
        serve()
    }

    sleep(1000*1000*1000*2)
    let rawUrl = "ws://0.0.0.0:9170"
    let headerMap = HashMap<String, String>()
    headerMap.put("tag","client")
    var wsclient = WebSocket4cjClient(rawUrl,headerMap)
    let connClient:WsConnClient = wsclient.connect().getOrThrow()
    let str = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
    connClient.sendMsg(str.toUtf8Array())
    let data = connClient.readMsg()
    let messType = data[0]
    if(messType.getValue() == 1){
        println("这是文本")
        match (data[1]) {
            case Some(msg)=>
                serverStr = String.fromUtf8(msg)
                println("msg:${String.fromUtf8(msg)}")
            case _ =>
                connClient.close()
                return 1
        }
    }

    if(str.equals(clientStr) && str.equals(serverStr)){
        return 0
    }else{
        return 1
    }

}

public func serve(){
    var ws = WebSocket4cjServer("0.0.0.0",9170)
    ws.handleFunc{w=>
        while (true) {
            let data = w.readMsg()
            match (data[1]) {
                case Some(msg)=>
                    clientStr = String.fromUtf8(msg)
                    println("msg:${String.fromUtf8(msg)}")
                    w.sendMsg(msg)
                case _ =>
                    w.close()
                    return
            }
        }
    }
    ws.launch() // 启动监听
}
```

执行结果如下：

```shell
msg:abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
这是文本
msg:abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz

```

#### 2.2 客户端与服务端的信息通信，包括发送二进制
客户端和服务端可以互相发送二进制

##### 2.2.1 主要接口

class WsConnClient

```cangjie
    /*客户端发送二进制消息的方法
     *
     *参数 arrayToWrite - 需要发送的数据
     */
    public func sendBinary(arrayToWrite: Array<UInt8>)

    /*客户端读取服务端发送过来消息的方法
     *
     * 返回值 - 返回一个元组类型 第一个参数是消息类型  第二个参数是读取到的字节数组
     */
    public func readMsg(): (MsgType,Option<Array<UInt8>>)
```

class WsConn

```cangjie
     /*服务端发送二进制消息的方法
     *
     *参数 arrayToWrite - 需要发送的数据
     */
    public func sendBinary(arrayToWrite: Array<UInt8>)

     /*服务端读取客户端发送过来消息的方法
     *
     * 返回值- 返回一个元组类型 第一个参数是消息类型  第二个参数是读取到的字节数组
     */
    public func readMsg(): (MsgType,Option<Array<UInt8>>)
```

##### 2.2.1.1 示例

```cangjie

from websocket4cj import client.*
from std import collection.*
from websocket4cj import server.*

var clientStr = ""
var serverStr = ""

main(){

    spawn{
        serve()
    }

    sleep(1000*1000*1000*2)
    let rawUrl = "ws://0.0.0.0:9170"
    let headerMap = HashMap<String, String>()
    headerMap.put("tag","client")
    var wsclient = WebSocket4cjClient(rawUrl,headerMap)
    let connClient:WsConnClient = wsclient.connect().getOrThrow()
    let str = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
    connClient.sendBinary(str.toUtf8Array())
    let data = connClient.readMsg()
    let messType = data[0]
    if(messType.getValue() == 2){
        println("这是二进制")
        match (data[1]) {
            case Some(msg)=>
                serverStr = String.fromUtf8(msg)
                println("msg:${String.fromUtf8(msg)}")
            case _ =>
                connClient.close()
                return 1
        }
    }

    if(str.equals(clientStr) && str.equals(serverStr)){
        return 0
    }else{
        return 1
    }
}

public func serve(){
    var ws = WebSocket4cjServer("0.0.0.0",9170)
    ws.handleFunc{w=>
        while (true) {
            let data = w.readMsg()
            match (data[1]) {
                case Some(msg)=>
                    clientStr = String.fromUtf8(msg)
                    println("msg:${String.fromUtf8(msg)}")
                    w.sendBinary(msg)
                case _ =>
                    w.close()
                    return
            }
        }
    }
    ws.launch() // 启动监听
}

```

执行结果如下：

```shell
msg:abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
这是二进制
msg:abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz

```

#### 2.3 客户端与服务端的信息通信，包括发送连续帧
客户端和服务端可以互相发送连续帧

##### 2.3.1 主要接口

class WsConnClient

```cangjie
    /*客户端写分片文本消息发送到服务端的方法
     *
     *参数 arrayToWriteList - 需要发送的分片数据集合
     */
    public func sendFragmentMsg(arrayToWriteList: ArrayList<Array<UInt8>>)

    /*客户端向服务端发送分片二进制的消息
     *
     * 参数 arrayToWriteList - 要写给服务端的数据集合
     */
    public func sendFragmentBinary(arrayToWriteList: ArrayList<Array<UInt8>>)

     /*客户端读取服务端发送过来消息的方法
     *
     * 返回值 - 返回一个元组类型 第一个参数是消息类型  第二个参数是读取到的字节数组
     */
    public func readMsg(): (MsgType,Option<Array<UInt8>>)
```

class WsConn

```cangjie
     /*服务端写分片文本消息发送到客户端的方法
     *
     *参数 arrayToWriteList - 需要发送的分片数据集合
     */
    public func sendFragmentMsg(arrayToWriteList: ArrayList<Array<UInt8>>)

    /*
     * 服务端向客户端发送分片二进制的消息
     * 参数 arrayToWriteList - 要写给客户端的数据集合
     */
    public func sendFragmentBinary(arrayToWriteList: ArrayList<Array<UInt8>>)

     /*服务端读取客户端发送过来消息的方法
     *
     * 返回值 - 返回一个元组类型 第一个参数是消息类型  第二个参数是读取到的字节数组
     */
    public func readMsg(): (MsgType,Option<Array<UInt8>>)

```

##### 2.3.1.1 示例

```cangjie

from websocket4cj import client.*
from std import collection.*
from websocket4cj import server.*

var serverStr = ""

main(){

    spawn{
        serve()
    }

    sleep(1000*1000*1000*2)
    let rawUrl = "ws://0.0.0.0:9170"
    let headerMap = HashMap<String, String>()
    headerMap.put("tag","client")
    var wsclient = WebSocket4cjClient(rawUrl,headerMap)
    let connClient:WsConnClient = wsclient.connect().getOrThrow()

    var listFrag = ArrayList<Array<UInt8>>()
    listFrag.append("你在".toUtf8Array())
    listFrag.append("家里".toUtf8Array())
    listFrag.append("了吗".toUtf8Array())
    listFrag.append("是的".toUtf8Array())
    listFrag.append("我在".toUtf8Array())
    listFrag.append("家里".toUtf8Array())
    listFrag.append("了呢。".toUtf8Array())
    connClient.sendFragmentMsg(listFrag)
    let data = connClient.readMsg()
    let messType = data[0]
    if(messType.getValue() == 1){
        println("这是文本")
    }else if(messType.getValue() == 2){
        println("这是二进制")
    }
    match (data[1]) {
        case Some(msg)=>
            serverStr = String.fromUtf8(msg)
            println("msgclient:${String.fromUtf8(msg)}")
        case _ =>
            connClient.close()
            return 1
    }

    if("你在家里了吗是的我在家里了呢。".equals(serverStr)){
        return 0
    }else{
        return 1
    }

}

public func serve(){
    var ws = WebSocket4cjServer("0.0.0.0",9170)
    ws.handleFunc{w=>
        while (true) {
            let data = w.readMsg()
            let messType = data[0]
            if(messType.getValue() == 1){
                println("这是文本")
            }else if(messType.getValue() == 2){
                println("这是二进制")
            }
            match (data[1]) {
                case Some(msg)=>
                    println("msgserver:${String.fromUtf8(msg)}")
                    var listFrag = ArrayList<Array<UInt8>>()
                    listFrag.append("你在".toUtf8Array())
                    listFrag.append("家里".toUtf8Array())
                    listFrag.append("了吗".toUtf8Array())
                    listFrag.append("是的".toUtf8Array())
                    listFrag.append("我在".toUtf8Array())
                    listFrag.append("家里".toUtf8Array())
                    listFrag.append("了呢。".toUtf8Array())
                    w.sendFragmentMsg(listFrag)
                case _ =>
                    w.close()
                    return
            }
        }
    }
    ws.launch() // 启动监听
}

```

执行结果如下：

```shell

这是文本
msgserver:你在家里了吗是的我在家里了呢。
这是文本
msgclient:你在家里了吗是的我在家里了呢。

```

##### 2.3.1.2 示例
```cangjie

from websocket4cj import client.*
from std import collection.*
from websocket4cj import server.*

var serverStr = ""

main(){

    spawn{
        serve()
    }

    sleep(1000*1000*1000*2)
    let rawUrl = "ws://0.0.0.0:9170"
    let headerMap = HashMap<String, String>()
    headerMap.put("tag","client")
    var wsclient = WebSocket4cjClient(rawUrl,headerMap)
    let connClient:WsConnClient = wsclient.connect().getOrThrow()

    var listFrag = ArrayList<Array<UInt8>>()
    listFrag.append("你在".toUtf8Array())
    listFrag.append("家里".toUtf8Array())
    listFrag.append("了吗".toUtf8Array())
    listFrag.append("是的".toUtf8Array())
    listFrag.append("我在".toUtf8Array())
    listFrag.append("家里".toUtf8Array())
    listFrag.append("了呢。".toUtf8Array())
    connClient.sendFragmentBinary(listFrag)
    let data = connClient.readMsg()
    let messType = data[0]
    if(messType.getValue() == 1){
        println("这是文本")
    }else if(messType.getValue() == 2){
        println("这是二进制")
    }
    match (data[1]) {
        case Some(msg)=>
            serverStr = String.fromUtf8(msg)
            println("msgclient:${String.fromUtf8(msg)}")
        case _ =>
            connClient.close()
            return 1
    }

    if("你在家里了吗是的我在家里了呢。".equals(serverStr)){
        return 0
    }else{
        return 1
    }
}

public func serve(){
    var ws = WebSocket4cjServer("0.0.0.0",9170)
    ws.handleFunc{w=>
        while (true) {
            let data = w.readMsg()
            let messType = data[0]
            if(messType.getValue() == 1){
                println("这是文本")
            }else if(messType.getValue() == 2){
                println("这是二进制")
            }
            match (data[1]) {
                case Some(msg)=>
                    println("msgserver:${String.fromUtf8(msg)}")
                    var listFrag = ArrayList<Array<UInt8>>()
                    listFrag.append("你在".toUtf8Array())
                    listFrag.append("家里".toUtf8Array())
                    listFrag.append("了吗".toUtf8Array())
                    listFrag.append("是的".toUtf8Array())
                    listFrag.append("我在".toUtf8Array())
                    listFrag.append("家里".toUtf8Array())
                    listFrag.append("了呢。".toUtf8Array())
                    w.sendFragmentBinary(listFrag)
                case _ =>
                    w.close()
                    return
            }
        }
    }
    ws.launch() // 启动监听
}

```
执行结果如下：

```shell
这是二进制
msgserver:你在家里了吗是的我在家里了呢。
这是二进制
msgclient:你在家里了吗是的我在家里了呢。

```
#### 2.4 客户端与服务端的信息通信，包括发送ping帧
客户端和服务端可以互相发送ping帧

##### 2.4.1 主要接口

class WsConnClient

```cangjie
    /*
     * 客户端向服务端发送ping的消息
     */
    public func sendPing()

    /*客户端读取服务端发送过来消息的方法
     *
     * 返回值 - 返回一个元组类型 第一个参数是消息类型  第二个参数是读取到的字节数组
     */
    public func readMsg(): (MsgType,Option<Array<UInt8>>)
```

class WsConn

```cangjie
     /*
     * 服务端向客户端发送ping的消息
     */
    public func sendPing()

     /*服务端读取客户端发送过来消息的方法
     *
     * 返回值 - 返回一个元组类型 第一个参数是消息类型  第二个参数是读取到的字节数组
     */
    public func readMsg(): (MsgType,Option<Array<UInt8>>)
```

##### 2.4.1.1 示例

```cangjie

from websocket4cj import client.*
from std import collection.*
from websocket4cj import server.*
from std import fs.*
from std import log.*
from std import io.*

var clientStr = ""
var serverStr = ""

main(){

    spawn{
        serve()
    }

    sleep(1000*1000*1000*2)
    let rawUrl = "ws://0.0.0.0:9170"
    let headerMap = HashMap<String, String>()
    headerMap.put("tag","client")
    var wsclient = WebSocket4cjClient(rawUrl,headerMap)
    let connClient:WsConnClient = wsclient.connect().getOrThrow()
    connClient.sendPing()
    let data = connClient.readMsg()
    match (data[1]) {
        case Some(msg)=>
            serverStr = String.fromUtf8(msg)
            println("msgclient:${String.fromUtf8(msg)}")
        case _ =>
            connClient.close()
            return 1
    }

    if("ping".equals(clientStr)&&"pong".equals(serverStr)){
        return 0
    }else{
        return 1
    }

}

public func serve(){
    var ws = WebSocket4cjServer("0.0.0.0",9170)
    ws.handleFunc{w=>
        while (true) {
            let data = w.readMsg()
            match (data[1]) {
                case Some(msg)=>
                    clientStr = String.fromUtf8(msg)
                    println("msgserver:${String.fromUtf8(msg)}")
                case _ =>
                    w.close()
                    return
            }
        }
    }
    ws.launch() // 启动监听
}

```

执行结果如下：

```shell

msgserver:ping
msgclient:pong

```

##### 2.4.1.2 示例

```cangjie

from websocket4cj import client.*
from std import collection.*
from websocket4cj import server.*

var clientStr = ""
var serverStr = ""

main(){

    spawn{
        serve()
    }

    sleep(1000*1000*1000*2)
    let rawUrl = "ws://0.0.0.0:9170"
    let headerMap = HashMap<String, String>()
    headerMap.put("tag","client")
    var wsclient = WebSocket4cjClient(rawUrl,headerMap)
    let connClient:WsConnClient = wsclient.connect().getOrThrow()
    let str = "你好啊haha"
    connClient.sendMsg(str.toUtf8Array())
    let data = connClient.readMsg()
    match (data[1]) {
        case Some(msg)=>
            serverStr = String.fromUtf8(msg)
            println("msgclient:${String.fromUtf8(msg)}")
            sleep(1000*1000*1000*5)
        case _ =>
            connClient.close()
            return 1
    }

    if("ping".equals(serverStr) && "pong".equals(clientStr)){
        return 0
    }else{
        return 1
    }

}

public func serve(){
    var ws = WebSocket4cjServer("0.0.0.0",9170)
    ws.handleFunc{w=>
        while (true) {
            let data = w.readMsg()
            match (data[1]) {
                case Some(msg)=>
                    clientStr = String.fromUtf8(msg)
                    println("msgserver:${String.fromUtf8(msg)}")
                    w.sendPing()
                case _ =>
                    w.close()
                    return
            }
        }
    }
    ws.launch() // 启动监听
}

```

执行结果如下：

```shell

msgserver:你好啊haha
msgclient:ping
msgserver:pong

```

#### 2.5 客户端与服务端的信息通信，包括发送close帧
客户端和服务端可以互相发送close帧

##### 2.5.1 主要接口

class WsConnClient

```cangjie
    /*
     * 客户端向服务端发送关闭的消息
     */
    public func sendClose()

    /*
     * 判断客户端socket有没有关的方法
     * 返回值 - 返回是否关闭 true表示已关 false表示未关
     */
    public func isClosed():Bool{

    /*客户端读取服务端发送过来消息的方法
     *
     * 返回值 - 返回一个元组类型 第一个参数是消息类型  第二个参数是读取到的字节数组
     */
    public func readMsg(): (MsgType,Option<Array<UInt8>>)
```

class WsConn

```cangjie
    /*
     * 服务端向客户端发送关闭的消息
     */
    public func sendClose()

    /*
     * 判断服务端socket有没有关的方法
     * 返回值 - 返回是否关闭 true表示已关 false表示未关
     */
    public func isClosed():Bool{

     /*服务端读取客户端发送过来消息的方法
     *
     * 返回值 - 返回一个元组类型 第一个参数是消息类型  第二个参数是读取到的字节数组
     */
    public func readMsg(): (MsgType,Option<Array<UInt8>>)
```

##### 2.5.1.1 示例

```cangjie

from websocket4cj import client.*
from std import collection.*
from websocket4cj import server.*

var clientStr = ""
var isClosed = false

main(){

    spawn{
        serve()
    }

    sleep(1000*1000*1000*2)
    let rawUrl = "ws://0.0.0.0:9170"
    let headerMap = HashMap<String, String>()
    headerMap.put("tag","client")
    var wsclient = WebSocket4cjClient(rawUrl,headerMap)
    let connClient:WsConnClient = wsclient.connect().getOrThrow()
    connClient.sendClose()
    sleep(1000*1000*1000*1)
    if("close".equals(clientStr)&&isClosed){
        return 0
    }else{
        return 1
    }
}

public func serve(){
    var ws = WebSocket4cjServer("0.0.0.0",9170)
    ws.handleFunc{w=>
        while (true) {
            let data = w.readMsg()
            match (data[1]) {
                case Some(msg)=>
                    clientStr = String.fromUtf8(msg)
                    isClosed = w.isClosed()
                    println("msgserver:${String.fromUtf8(msg)}")
                case _ =>
                    w.close()
                    return
            }
        }
    }
    ws.launch() // 启动监听
}

```

执行结果如下：

```shell
msgserver:close

```

##### 2.5.1.2 示例

```cangjie

from websocket4cj import client.*
from std import collection.*
from websocket4cj import server.*

var serverStr = ""
var isClosed = false

main(){

    spawn{
        serve()
    }

    sleep(1000*1000*1000*2)
    let rawUrl = "ws://0.0.0.0:9170"
    let headerMap = HashMap<String, String>()
    headerMap.put("tag","client")
    var wsclient = WebSocket4cjClient(rawUrl,headerMap)
    let connClient:WsConnClient = wsclient.connect().getOrThrow()
    let str = "你好啊haha"
    connClient.sendMsg(str.toUtf8Array())
    let data = connClient.readMsg()
    match (data[1]) {
        case Some(msg)=>
            serverStr = String.fromUtf8(msg)
            isClosed = connClient.isClosed()
            println("msgclient:${String.fromUtf8(msg)}")
        case _ =>
            connClient.close()
            return 1
    }

    if("close".equals(serverStr)&&isClosed){
        return 0
    }else{
        return 1
    }

}

public func serve(){
    var ws = WebSocket4cjServer("0.0.0.0",9170)
    ws.handleFunc{w=>
        while (true) {
            let data = w.readMsg()
            match (data[1]) {
                case Some(msg)=>
                    println("msgserver:${String.fromUtf8(msg)}")
                    w.sendClose()
                case _ =>
                    w.close()
                    return
            }
        }
    }
    ws.launch() // 启动监听
}

```

执行结果如下：

```shell
msgserver:你好啊haha
msgclient:close

```

## 其他接口

### 介绍

    这些接口在内部流程中使用, 无需调用者操作, 也有可能随迭代修改其实现，请按需谨慎使用。

### 主要接口

#### SHA1

```cangjie
/*
 * SHA1算法
 */
public class SHA1 {
    /*
     * 生成对于字符串的SHA1值
     * 参数 src - 原字符串
     * 返回值 - 返回生成的字节数组
     */
    public static func digest(src: String): Array<UInt8>
}
```

