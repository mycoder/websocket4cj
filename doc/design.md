# websocket4cj

## 描述
一个拥有websocket功能的仓颉版本

## 类描述

### class WebSocket4cjServer

websocket4cj服务端类，用于启动websocket4cj服务端

```cangjie
public class WebSocket4cjServer{

    /*创建WebSocket4cjServer对象
     *
     *参数 host - 主机名
     *参数 port - 端口号
     */
    public init(host:String,port:UInt16)

    /*用于启动服务端websocket4cj
     *
     */
    public func launch()

    /*用连接后的socket对象操作的函数
     *
     *参数 fn - 参数为WsConn没有返回值的函数，拿到连接对象进行操作
     */
    public func handleFunc(fn:(WsConn)->Unit)

    /*用于设置host
     *
     *参数 host - 主机名
     */
    public func setHost(host:String)

    /*用于设置port
     *
     *参数 port - 端口号
     */
    public func setPort(port:UInt16)

    /*用于设置是否阻塞主线程
     *
     *参数 no_blocking - true不阻塞 false阻塞
     */
    public func setNoBlocking(no_blocking:Bool)

}
```

```cangjie
public enum MsgType{

    /*用于获取服务端对应消息类型的Int64值
     *
     * 返回值 - 返回消息枚举类型对应的Int64值
     */
    public func getValue(): Int64
}
```

```cangjie
public class WsConn{

     /*服务端读取客户端发送过来消息的方法
      *
      *返回值 - 返回元组类型 第一个参数 消息类型  第二个参数 读取的数据
      */
     public func readMsg(): (MsgType,Option<Array<UInt8>>)

    /*服务端写消息发送到客户端的方法
     *
     *参数 arrayToWrite - 需要发送的数据
     */
    public func sendMsg(arrayToWrite: Array<UInt8>)

    /*服务端写分片文本消息发送到客户端的方法
     *
     *参数 arrayToWriteList - 需要发送的分片数据集合
     */
    public func sendFragmentMsg(arrayToWriteList: ArrayList<Array<UInt8>>)

    /*服务端发送二进制消息的方法
     *
     *参数 arrayToWrite - 需要发送的数据
     */
    public func sendBinary(arrayToWrite: Array<UInt8>)

    /*服务端写分片二进制消息到客户端的方法
     *
     *参数 arrayToWriteList - 需要发送的分片数据集合
     */
    public func sendFragmentBinary(arrayToWriteList: ArrayList<Array<UInt8>>)

    /*服务端发送ping消息的方法
     *
     */
    public func sendPing()

    /* 判断服务端socket有没有关的方法
     *
     * 返回值 - 返回是否关闭 true表示已关 false表示未关
     */
    public func isClosed():Bool

    /*服务端发送结束连接消息的方法
     *
     */
     public func sendClose()

    /*服务端关闭连接的方法
     *
     */
    public func close()

    /*服务端获取客户端Method
     *返回值 - 返回获取到的客户端Method
     */
    public func getMethod():String

    /*服务端获取客户端url
     *返回值 - 返回获取到的客户端url
     */
    public func getUrl():String

    /*服务端获取客户端HttpVersion
     *返回值 - 返回获取到的客户端HttpVersion
     */
    public func getHttpVersion():String

    /*服务端获取客户端传过来Header
     *返回值 - 返回获取到的客户端Header
     */
    public func getHeader():HashMap<String, String>{

}
```

```cangjie
public class WebSocket4cjClient{

    /*创建WebSocket4cjClient对象
     *
     *参数 rawUrl -服务端地址url
     *参数 headers - 请求头
     */
    public init(rawUrl:String,headers:HashMap<String, String>)

    /*创建WebSocket4cjClient对象
     *
     *参数 rawUrl -服务端地址url
     *参数 headers - 请求头
     *参数 timeOutTime - 超时时长单位秒，时长必须大于0
     */
    public init(rawUrl:String,headers:HashMap<String, String>,timeOutTime:Int64)

    /*客户端连接的方法
     *
     *返回值 - 返回一个握手成功后返回的连接对象
     */
    public func connect():Option<WsConnClient>

}
```

```cangjie
public enum MsgType{

    /*用于获取客户端对应消息类型的Int64值
     *
     * 返回值 - 返回消息枚举类型对应的Int64值
     */
    public func getValue(): Int64
}
```

```cangjie
public class WsConnClient{

    /*客户端读取服务端发送过来消息的方法
     *
     *返回值 - 返回一个元组类型 第一个参数 消息类型  第二个参数 读取的数据
     */
     public func readMsg(): (MsgType,Option<Array<UInt8>>)

    /*客户端写消息发送到服务端的方法
     *
     *参数 arrayToWrite -  需要发送的数据
     */
    public func sendMsg(arrayToWrite: Array<UInt8>)

    /*客户端写分片文本消息发送到服务端的方法
     *
     *参数 arrayToWriteList - 需要发送的分片数据集合
     */
    public func sendFragmentMsg(arrayToWriteList: ArrayList<Array<UInt8>>)

    /*客户端发送二进制消息的方法
     *
     *参数 arrayToWrite - 需要发送的数据
     */
    public func sendBinary(arrayToWrite: Array<UInt8>)

    /*客户端写分片二进制消息到服务端的方法
     *
     *参数 arrayToWriteList - 需要发送的分片数据集合
     */
    public func sendFragmentBinary(arrayToWriteList: ArrayList<Array<UInt8>>)

    /*客户端发送ping消息的方法
     *
     */
    public func sendPing()

    /*判断客户端socket有没有关的方法
     *
     * 返回值 - 返回是否已经关闭 true表示已关 false表示未关
     */
    public func isClosed():Bool

    /*客户端发送结束连接消息的方法
     *
     */
     public func sendClose()

    /*客户端关闭连接的方法
     *
     */
    public func close()

}
```

##  展示示例

####  握手逻辑

```

from websocket4cj import client.*
from std import collection.*
from websocket4cj import server.*

var method = ""
var httpversion = ""
var headers = HashMap<String,String>()

main(){
    spawn{
        serve()
    }

    sleep(1000*1000*1000*1)
    let rawUrl = "ws://0.0.0.0:9170"
    let headerMap = HashMap<String, String>()
    headerMap.put("tag","client")
    var wsclient = WebSocket4cjClient(rawUrl,headerMap,10)
    let connClient:WsConnClient = wsclient.connect().getOrThrow()
    println("客户端握手成功")
    sleep(1000*1000*1000*1)
    if("GET".equals(method) && "HTTP/1.1".equals(httpversion) && headers.size>0){
        return 0
    }else{
        return 1
    }
}

public func serve(){
    var ws = WebSocket4cjServer("0.0.0.0",9170)
    ws.handleFunc{w=>
         method = w.getMethod()
         httpversion = w.getHttpVersion()
         headers = w.getHeader()
        println("method="+method)
        println("httpversion="+httpversion)
        for(head in headers){
            println("key="+head[0]+" value="+head[1])
        }
        println("服务端握手成功")
    }
    ws.launch() // 启动监听
}

```

```cangjie
执行结果:

method=GET
httpversion=HTTP/1.1
key=Host value=0.0.0.0
key=Upgrade value=websocket
key=Connection value=Upgrade
key=Sec-WebSocket-Key value=bVZ3UnJiT0J0RmNISkJPZA==
key=Origin value=ws
key=tag value=client
key=Sec-WebSocket-Version value=13
服务端握手成功
客户端握手成功

```

####  客户端服务端收发消息逻辑

```

from websocket4cj import client.*
from std import collection.*
from websocket4cj import server.*
from std import time.*
from std import sync.*

var port: UInt16 = 2029
var startTime : Int64 = 0
var endTime : Int64 = 0

var i = 0
var j = 0

let rawUrl = "ws://0.0.0.0:${port}"
let headerMap = HashMap<String, String>()
var wsclient = WebSocket4cjClient(rawUrl,headerMap)
var conn:Option<WsConnClient> = Option<WsConnClient>.None

//该用例是测试性能时候用

main(){
    spawn{
        serve()
    }
    sleep(1000*1000*1000*2)
    conn = wsclient.connect()
    println("连接成功")
    spawn{
        while(j<100001){
            if(j == 0){
                startTime = Time.now().unixTimeStamp().nanoseconds()
            }
            let str = "窗前明月光疑是地上霜举头望明月低头思故乡窗前明月光疑是地上霜举头望明月低头思故乡窗前明月光疑是地上霜举头望明月低头思故乡窗前明月光疑是地上霜举头望明月低头思故乡窗前明月光疑是地上霜举头望明月低头思故乡"+j.toString()
            conn.getOrThrow().sendMsg(str.toUtf8Array())
            j++
        }
    }
    while(i<100001){
        let data = conn.getOrThrow().readMsg()
        i++
        match (data[1]) {
            case Some(msg)=>
                if(i == 100000){
                    endTime = Time.now().unixTimeStamp().nanoseconds()
                    println("用时----"+(endTime-startTime).toString())
                }
                //println("msgserver:${String.fromUtf8(msg)}")
            case _ =>
                println("断了")
                conn.getOrThrow().close()
                return
        }
    }
}

 public func serve(){
        var ws = WebSocket4cjServer("0.0.0.0",port)
        ws.handleFunc{w=>
            while (true) {
                let data = w.readMsg()
                match (data[1]) {
                    case Some(msg)=>
                        w.sendMsg(msg)
                    case _ =>
                        w.close()
                        return
                }
            }
        }
        ws.launch() // 启动监听
    }

```

```cangjie

执行结果:

连接成功
用时----1052311200

```
