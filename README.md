<div align="center">
<h1>websocket4cj</h1>
</div>


<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v0.0.4-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.39.4-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-86.2%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>



## <img alt="" src="./doc/assets/readme-icon-introduction.png" style="display: inline-block;" width=3%/> 介绍
仓颉的websocket库

### 特性

- 🚀 websocket协议的实现，无冗余代码

- 🚀 纯仓颉开发，无额外引用

- 🚀  代码简单、精简，可方便学习仓颉和扩展需要的功能

## <img alt="" src="./doc/assets/readme-icon-framework.png" style="display: inline-block;" width=3%/> 软件架构

### 架构图

<p align="center">
<img src="./doc/assets/classrelation.png" width="40%" >
</p>


### 源码目录
```shell
.
├── README.md
├── module.json
├── LICENSE
├── CHANGELOG.md
├── gitee_gate.cfg
├── README.OPENSOURCE
├── doc
│   ├── assets
│   ├── cjcov
│   ├── api.md
│   ├── design.md
│   ├── LICENSE
│   ├── websocket4cj核心类New.xmind
│   └── websocket4cj类关系图New.xmind
│
├── src
│   └── server
│       ├── client.cj
│       ├── frame_header.cj
│       ├── request.cj
│       ├── response.cj
│       ├── status_code.cj
│       ├── upgrader.cj
│       ├── ws_conn.cj
│       └── web_socket_server.cj
│   └── client
│       ├── frame_header.cj
│       ├── shark_hand.cj
│       ├── shark_request.cj
│       ├── shark_response.cj
│       ├── status_code.cj
│       ├── web_socket_client.cj
│       └── ws_conn_client.cj
│   └── utils
│       └── sha1.cj
└── test
```
- `doc`  文档资源目录,存放了图片资源，覆盖率报告，api文档，设计文档，核心类说明和类关系图还有LICENSE
- `src`  库的源码目录
- `test` 存放了测试用例

### 接口说明

主要是核心类和成员函数说明，详情请见  [设计文档](./doc/design.md)

## <img alt="" src="./doc/assets/readme-icon-compile.png" style="display: inline-block;" width=3%/> 使用说明


### 编译

#### Linux编译

```
git clone https://gitee.com/HW-PLLab/websocket4cj.git
地址：https://gitee.com/HW-PLLab/testJekins 将 src 下 ci_test 放入 websocket4cj 根目录下
python3 ci_test/main.py build
```

#### Windows编译

```
1.获取最新tag的websocket4cj代码
2.编译websocket4cj
  切换到websocket4cj下，即src同级目录，执行如下命令，提示success即表示编译成功
     cpm build
```

### 功能示例

####  握手逻辑

```

from websocket4cj import client.*
from std import collection.*
from websocket4cj import server.*

var method = ""
var httpversion = ""
var headers = HashMap<String,String>()

main(){
    spawn{
        serve()
    }

    sleep(1000*1000*1000*1)
    let rawUrl = "ws://0.0.0.0:9170"
    let headerMap = HashMap<String, String>()
    headerMap.put("tag","client")
    var wsclient = WebSocket4cjClient(rawUrl,headerMap,10)
    let connClient:WsConnClient = wsclient.connect().getOrThrow()
    println("客户端握手成功")
    sleep(1000*1000*1000*1)
    if("GET".equals(method) && "HTTP/1.1".equals(httpversion) && headers.size>0){
        return 0
    }else{
        return 1
    }
}

public func serve(){
    var ws = WebSocket4cjServer("0.0.0.0",9170)
    ws.handleFunc{w=>
         method = w.getMethod()
         httpversion = w.getHttpVersion()
         headers = w.getHeader()
        println("method="+method)
        println("httpversion="+httpversion)
        for(head in headers){
            println("key="+head[0]+" value="+head[1])
        }
        println("服务端握手成功")
    }
    ws.launch() // 启动监听
}

```

执行结果如下：

```
method=GET
httpversion=HTTP/1.1
key=Host value=0.0.0.0
key=Upgrade value=websocket
key=Connection value=Upgrade
key=Sec-WebSocket-Key value=bVZ3UnJiT0J0RmNISkJPZA==
key=Origin value=ws
key=tag value=client
key=Sec-WebSocket-Version value=13
服务端握手成功
客户端握手成功

```

####  客户端服务端收发消息逻辑

```

from websocket4cj import client.*
from std import collection.*
from websocket4cj import server.*
from std import time.*
from std import sync.*

var port: UInt16 = 2029
var startTime : Int64 = 0
var endTime : Int64 = 0

var i = 0
var j = 0

let rawUrl = "ws://0.0.0.0:${port}"
let headerMap = HashMap<String, String>()
var wsclient = WebSocket4cjClient(rawUrl,headerMap)
var conn:Option<WsConnClient> = Option<WsConnClient>.None

main(){
    spawn{
        serve()
    }
    sleep(1000*1000*1000*2)
    conn = wsclient.connect()
    println("连接成功")
    spawn{
        while(j<100001){
            if(j == 0){
                startTime = Time.now().unixTimeStamp().nanoseconds()
            }
            let str = "窗前明月光疑是地上霜举头望明月低头思故乡窗前明月光疑是地上霜举头望明月低头思故乡窗前明月光疑是地上霜举头望明月低头思故乡窗前明月光疑是地上霜举头望明月低头思故乡窗前明月光疑是地上霜举头望明月低头思故乡"+j.toString()
            conn.getOrThrow().sendMsg(str.toUtf8Array())
            j++
        }
    }
    while(i<100001){
        let data = conn.getOrThrow().readMsg()
        i++
        match (data[1]) {
            case Some(msg)=>
                if(i == 100000){
                    endTime = Time.now().unixTimeStamp().nanoseconds()
                    println("用时----"+(endTime-startTime).toString())
                }
                //println("msgserver:${String.fromUtf8(msg)}")
            case _ =>
                println("断了")
                conn.getOrThrow().close()
                return
        }
    }
}

 public func serve(){
        var ws = WebSocket4cjServer("0.0.0.0",port)
        ws.handleFunc{w=>
            while (true) {
                let data = w.readMsg()
                match (data[1]) {
                    case Some(msg)=>
                        w.sendMsg(msg)
                    case _ =>
                        w.close()
                        return
                }
            }
        }
        ws.launch() // 启动监听
    }

```

执行结果如下：

```
 连接成功
 用时----1052311200

```

### 重要说明
- 这个库的协议握手部分实现是参照`gorilla/websocket`进行的
- 该库为websocket协议的实现（主要为协议握手、帧解析、包装等功能）

## <img alt="" src="./doc/assets/readme-icon-contribute.png" style="display: inline-block;" width=3%/> 参与贡献

欢迎给我们提交PR，欢迎给我们提交Issue，欢迎参与任何形式的贡献。

